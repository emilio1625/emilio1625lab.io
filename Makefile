init:	.gitmodules
	echo init update | \
	  xargs -t -n 1 git submodule

run:	init
	hugo server \
	  --appendPort=false \
	  --disableFastRender
